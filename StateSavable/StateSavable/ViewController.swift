//
//  ViewController.swift
//  StateSavable
//
//  Created by Martin Burolla on 3/25/16.
//  Copyright © 2016 Martin Burolla. All rights reserved.
//

import UIKit

class ViewController: UIViewController, StateSavableable {

    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - User Interactions
    
    @IBAction func onPickGuests(sender: AnyObject) {
        saveState(PickerStates.GuestPicked)
    }

    @IBAction func onPickAdults(sender: AnyObject) {
        saveState(PickerStates.AdultsPicked)
    }
  
    @IBAction func onPickPets(sender: AnyObject) {
        saveState(PickerStates.PetsPicked)
    }
}
