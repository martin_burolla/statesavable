//
//  StateProtocols.swift
//  StateSavable
//
//  Created by Martin Burolla on 3/25/16.
//  Copyright © 2016 Martin Burolla. All rights reserved.
//

import Foundation

// MARK: All application states go here.

enum PickerStates {
    case Empty, GuestPicked, AdultsPicked, PetsPicked
}

// MARK: - Protocols

protocol State { }
protocol StateSavableable : State { }
protocol StateReadable : State { }

// MARK: - Implementations

extension StateSavableable {
    
    func saveState(state: PickerStates) {
        // Use Mirror to get class name.
        // Save the class name and the state to a persistent store.
    }
}

extension StateReadable {
    
    func readState() -> PickerStates {
        // Use Mirror to get class name.
        // Read the class name and the state from a persisent store.
        return PickerStates.GuestPicked
    }
}

